import argparse, pathlib
parser = argparse.ArgumentParser(
                formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                description='This program is for analysing data in the format: \
                [ID] | [Question] | [Answer] \
                Use the `--verbose\' flag to get more info about the it.')

def myParse():
    global parser
    # wanted to save graphs and open them later instead of saving images
    # not enough time to work on this feature so it's disabled
    #parser.add_argument('-open', '--open-figure',
    #            dest="openFigure",
    #            help="Used to specify the starting mode of the program to open a saved figure.",
    #            action='store_true')
    # basic
    parser.add_argument('-p', '--data-paths',
                nargs='+',
                dest="path",
                help="Used to specify one or multiple paths, relative or absolute, to the directory of \
                    pointing at the directory from which to load the data. Multiple data paths can be \
                    used. If not set, it defaults to current dirrectory. It does not support to be used \
                    with direct paths to files at the same time. Choose one.",
                default = None,
                type=pathlib.Path)
    parser.add_argument('-f', '--file-names',
                nargs='+',
                dest="files",
                help="Used to specify one or more files to be loaded from the specified path/s. If no path \
                    is specified, it will load files from the current dirrectory. It supports using direct paths \
                    to the files, instead of just specifying file names, however, the path parameter should \
                    not be given in that case. If no file is specified, it will load all files from the path. \
                    It can't load all files from current directory as a safety precaution. It can be used in \
                    combination with the extension argument, however, the file names should not include an \
                    extenstion in that case.",
                default=None)
    parser.add_argument('-ext', '--file-extensions',
                nargs='+',
                dest="fileExtensions",
                help="Used to specify a filter extension. Can be used with files and/or path. Do not specify \
                    an extension in the file name if used with this command. If no file is specified, it will \
                    load all files with the given extension/s from the given path. If no path is specified, \
                    it will use the current path.",
                default = None)
    parser.add_argument('-pm', '--perspective-mode',
                dest="mode",
                help="Used to specify the perspective from which to analyse the data. The program has 3 \
                    perspectives: (I) from the perspective of images, (Q) from the perspective of questions, \
                    (A) from the perspective of answers. Mentioning more than one, will make the program cycle \
                    through the available perspectives (ex: -pm QIA will execute Q, then I and then A).",
                default="Q",
                type=str)

    ### analyse
    parser.add_argument('-analyse', '--analyse-data',
                dest="analyseData",
                help="Used to specify the starting mode of the program to analyse the data.",
                action='store_true')

    # recursive
    parser.add_argument('-r', '--recursive',
                dest='recursive',
                help="Makes the program load the specified files recursively (one after another) and compute \
                    for each one the desired options.",
                action='store_true')
    parser.add_argument('-rm', '--merge-recursive-results',
                dest='recursiveMerge',
                help="Used with recursive. Makes the program do a recursive pass then an extra pass through all the \
                    data merged.",
                action='store_true')
    parser.add_argument('-exp', '--export-results',
                dest="exportResults",
                help="Used to specify whether to export the results to an xlx file or not.",
                action='store_true')
    parser.add_argument('-med', '--use-median',
                dest="useMedian",
                help="Used to specify whether to utilize a median to split the data. Everything under the value \
                    of the median is grouped together with an 'other' tag for distinction.",
                action='store_true')
    # thresholds
    parser.add_argument('-tP', '--threshold-IQA-parent-value',
                dest="thresholdParent",
                help="Gives a threshold value on which to split data (x). Defaults to 0 - no splitting.",
                default= 0,
                type=int)
    parser.add_argument('-tC', '--threshold-IQA-child-value',
                dest="thresholdChild",
                help="Gives a threshold value on which to split data (x). Defaults to 0 - no splitting.",
                default= 0,
                type=int)
    parser.add_argument('-tW', '--threshold-word-count-value',
                dest="thresholdWC",
                help="Gives a threshold value on which to split data (x). Defaults to 0 - no splitting.",
                default= 0,
                type=int)
    parser.add_argument('-t', '--threshold-values',
                nargs=3,
                dest="threshold",
                help="Used to give the threshold values for the IQA Parent, Child and the Word Count in an array [P,C,W]. \
                    The easier version from using -tP P -tC -C -tW W. Does the same thing.",
                default= None,
                type = int)
    # display
    parser.add_argument('-disp', '--show-results',
                dest="showResults",
                help="Used to specify whether to display the results or not.",
                action='store_true')
    parser.add_argument('-sI', '--show-images',
                dest="showImages",
                help="Used to specify whether to display the detailed results on images or not.",
                action='store_true')    
    parser.add_argument('-sQ', '--show-questions',
                dest="showQuestions",
                help="Used to specify whether to display the detailed results on Questions or not.",
                action='store_true')
    parser.add_argument('-sA', '--show-answers',
                dest="showAnswers",
                help="Used to specify whether to display the detailed results on Answers or not. ",
                action='store_true')
    # similarity
    parser.add_argument('-sm', '--similarity-method',
                dest="similarityMethod",
                help="Used to activate the computation of simiarity between data and to specify at the same \
                    time what similarity method to use: (1) GenSim method word2vec, (2) Window Method, \
                    (3) Token similarity.",
                default= None,
                type=int)
    parser.add_argument('-lem', '--lemmatizer-selector',
                dest="lemmatizer",
                help="Decide what lemmatizer to use. Available: 'WordNet'.",
                default= 'WordNet',
                type=str)
    parser.add_argument('-stem', '--stemmer-selector',
                dest="stemmer",
                help="Decide what stemmer to use. Available: 'Porter' and 'Lancaster'.",
                default= 'Porter',
                type=str)
    parser.add_argument('-dispSim', '--show-similarity-results',
                dest="showSimResults",
                help="Used to specify whether to display the results or not for the similarity.",
                action='store_true')
    # threshold similarity
    parser.add_argument('-tS', '--similarity-threshold',
                nargs='+',
                dest="thresholdSim",
                help="Gives a threshold value on which to split the similarity information.",
                default= [0.5],
                type=float)

    # arguments for the processing the data
    parser.add_argument('-process', '--process-data',
                dest="processData",
                help="Used to specify the starting mode of the program to process the data.",
                action='store_true')
    parser.add_argument('-pf', '--process-file',
                dest='processFile',
                help="Used to specify whether the program processes a file or not. It wwill transform each Q and A into \
                    a list of tokens and create a new file where it will save the result. With this command you can use\
                    -pfm lem, -pfm stem, -rmsw. Those arguments are optional. Extra, you can use -pfo to give an output file.",
                action='store_true')
    parser.add_argument('-pfm', '--process-file-mode',
                dest='processFileMode',
                help="Used to specify what method the program will use for the tokenizing of the words when using -pf.  \
                    Using 'lem' or 'stem' will tell the program to either lemmatize or stem the words. With this command you can use\
                    The argument is optional. If not specified the program will not stem nor lemmatize.",
                default= None,
                type=str)
    parser.add_argument('-rmsw', '--remove-stop-words',
                dest='removeStopWords',
                help="Used to specify whether the program will remove or not the stop words when processing the file",
                action='store_true')
    parser.add_argument('-pfsafe', '--process-file-safety',
                dest='processFileSafety',
                help="Used together with -rmsw. Will keep at least one word in the sentence if all of them are stopwords.",
                action='store_true')
    parser.add_argument('-pfo', '--process-file-output',
                dest='processFileOutput',
                help="Used to specify an output file for the result of the file processing.",
                default='processedFile.txt',
                type= pathlib.Path)
    # word representation
    parser.add_argument('-wr', '--word-representation',
                dest='wordRepresentation',
                help="Used to specify whether the program does the word representation of the data or not.\
                    The result is a plotted graph which has to be saved by the user.",
                action='store_true')
    parser.add_argument('-wrm', '--word-representation-model',
                dest='wordRepresentationModel',
                help="Used to specify whether the program does the word representation of the data or not.\
                    The result is a plotted graph which has to be saved by the user.",
                default = None,
                type=pathlib.Path)
    parser.add_argument('-wrc', '--word-representation-system-cores',
                dest="wordRepresentationCores",
                help="Used to specify how many cores can be used for the training model to get the word representation",
                default=2,
                type=int)
    parser.add_argument('-wrt', '--word-representation-threshold',
                dest="wordRepresentationThreshold",
                help="Used to specify the threshold value for displaying words on the graph. It reffers to their number of occurences \
                    in the given text to analyse. Useful especially when having your own model already trained",
                default=1,
                type=int)
    parser.add_argument('-wrmc', '--word-representation-min-count',
                dest="wordRepresentationMinCount",
                help="Used to specify the threshold value for the considered words when training your model. It reffers to the\
                    minimim number of their occurences. Anything below this value will not be considered",
                default=5,
                type=int)
    parser.add_argument('-wrml', '--word-representation-min-length',
                dest="wordRepresentationMinLength",
                help="Used to specify the threshold value for the considered words. It reffers to string length of each word \
                    in the given text to analyse. Anything below this value will not be considered",
                default=1,
                type=int)

    parser.add_argument('-wrws', '--word-representation-window-size',
                dest="wordRepresentationWindowSize",
                help="Used to specify the threshold value for the window size to be used when training your own model for word representation.",
                default=5,
                type=int)
    parser.add_argument('-wrsg', '--word-representation-skip-diagram',
                dest="wordRepresentationSG",
                help="Used to specify whether the program will use the skip diagram (1) or CBOW (0).",
                default=0,
                type=int)
    parser.add_argument('-wrvs', '--word-representation-vector-size',
                dest="wordRepresentationVectorSize",
                help="Used to specify the vector size for the training model",
                default=100,
                type=int)





    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true')
    return parser
def help():
    print(parser.parse_args())
    parser.print_help()
def main():
    myParse()
    help()
if __name__ == "__main__":
    main()