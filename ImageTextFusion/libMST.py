#Written for python3(.7)
#Import needed libraries
import nltk, re, gensim, collections, xlsxwriter, statistics, os
from sklearn.decomposition import PCA
from matplotlib import pyplot
#Set the colors for the terminal
cyan = "\033[96m"; blue = "\033[36m"; reset = "\033[0m";grey = "\033[90m"
#Symbolic variables
IMG = 0; Q = 1; A = 2
#implementations
modes = ['I','Q','A']
simMethods = ['gSim','tSim','wSim'] 
def getModes(i=None): return modes if i == None else modes[i]
def getSimMethods(i=None): return modes if i == None else simMethods[i]

class globalList:
    def __init__(self, dataval=None):
        self.byImage = [] #unique data with counter sorted by Image
        self.sizeByImage = []
        self.byQuestion = [] #unique data with counter sorted by Question
        self.sizeByQuestion = []
        self.byAnswer = [] #unique data with counter sorted by Answer
        self.sizeByAnswer = []
class globalHolder: # will hold all the data so I won't have to pass around too many arguments
    def __init__(self, dataval=None):
        self.Images = globalList()
        self.Questions = globalList()
        self.Answers = globalList()

# Function for loading the data
def loadData(fileNames):
    myData = [[' '.join(val.split()) for val in line.strip('(').strip(')').split('|')] for line in open(fileNames[0],'r').readlines()] #strip('\n').strip('\t').
    for i in range(1,len(fileNames),1):
        myData.extend([[' '.join(val.split()) for val in line.split('|')] for line in open(fileNames[i],'r').readlines()])
    return myData


### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
# * * * * * * * * * * * * LTK functions * * * * * * * * * * * 
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
class myLTK:
    def stringNormalizer(self,stringText):
        # takes a string and replaces special characters with spaces
        return ' '.join(re.sub(r'[^a-zA-Z0-9\s]+', ' ', stringText).split()).lower()

    def textNormalizer(self,stringList):
        #takes a list of strings and replaces special characters with spaces
        for i in range(0,len(stringList),1):
            stringList[i] = self.stringNormalizer(stringList[i])
        return stringList

    def sentenceStringTokenizer(self,sentence): #Creates tokens from a sentence string
        return nltk.tokenize.word_tokenize(sentence)

    def sentenceListTokenizer(self,sentencesList): # Creates tokens from a sentence list
        # Make empty list holder for the tokens of each sentence
        tokenizedSentences = ['' for i in range(0,len(sentencesList))]
        # Tokenize each sentence
        for i in range(0,len(sentencesList)):
            tokenizedSentences[i] = self.sentenceStringTokenizer(sentencesList[i])
        return tokenizedSentences

    def removeTokenStopWords(self,sentenceTokens,stopwords=nltk.corpus.stopwords.words('english'),safety = False): #removes the stop words from a list of tokens
        if safety:
            check = [word for word in sentenceTokens if not word in stopwords]
            if len(check) == 0:
                return sentenceTokens
            else: return check
        else: return [word for word in sentenceTokens if not word in stopwords]

    def removeSentencesListStopWords(self,tokenizedSentences,banList=[]):
        #removes the stopwords from a list of lists of tokens
        stopwords = nltk.corpus.stopwords.words('english')
        #update stopwords with personalised ban list
        stopwords.extend(banList)
        normalisedSentences = []
        #for each tokenized sentence in the list removes the stopwords
        for sentence in tokenizedSentences:
            normalisedSentences.append(self.removeTokenStopWords(sentence, stopwords))
        return normalisedSentences

    def wordStemmer(self,word,stemmer = "Porter"): #stems a word with the specified stemmer
        #Uses NLTK to stem words, either with Porter or Lancaster (agressive)
        if stemmer == "Porter": 
            return nltk.stem.PorterStemmer().stem(word)
        elif stemmer == "Lancaster":
            return nltk.stem.LancasterStemmer().stem(word)
        else: 
            print("Error in specifying the Stemmer. Default one used instead.")
            return nltk.stem.PorterStemmer().stem(word)

    def sentenceTokenStemmer(self,tokenizedSentence,stemmer = "Porter"):
        #stepms a tokenized sentence
        stemmedTokenizedSentence = []
        for word in tokenizedSentence:
                stemmedTokenizedSentence.extend([self.wordStemmer(word,stemmer)])
        return stemmedTokenizedSentence

    def sentenceListTokenStemmer(self,tokenizedSentences,stemmer="Porter"):
        #stems a list of tokenized sentences
        stemmedTokenizedSentences = []
        for sentence in tokenizedSentences:
            stemmedTokenizedSentences.append(self.sentenceTokenStemmer(sentence))
        return stemmedTokenizedSentences

    def wordLemmer(self,word,lemmatizer = "WordNet"):
        #Lemmatizes a word using the specified lemmatizer
        if lemmatizer == "WordNet":
            return nltk.stem.WordNetLemmatizer().lemmatize(word)
        else: 
            print("Error in specifying the Lemmatizer. Default one used instead.")
            return nltk.stem.WordNetLemmatizer().lemmatize(word)

    def sentenceTokenLemmer(self,tokenizedSentence,lemmatizer = "WordNet"): #lemmatizes a tokenized sentence
        lemmatiedTokenizedSentence = []
        for word in tokenizedSentence:
            lemmatiedTokenizedSentence.extend([self.wordLemmer(word,lemmatizer)])
        return lemmatiedTokenizedSentence

    def sentenceListTokenLemmer(self,tokenizedSentences,lemmatizer = "WordNet"): #Uses NLTK to stem words, either with Porter or Lancaster (agressive)
        #Must take a list of lists of strings as an argument
        # Stem the tokens of the list of lists
        lemmatizedTokenizedSentences = []
        for sentence in tokenizedSentences:
            lemmatizedTokenizedSentences.append(self.sentenceTokenLemmer(sentence,lemmatizer))
        return lemmatizedTokenizedSentences

    def getWordFrequencyFromTokenizedSentence(self,tokenizedSentence,wordsCount={}):
        for word in tokenizedSentence:
            if word in wordsCount:
                wordsCount[word] += 1
            else: wordsCount[word] = 1
        return wordsCount
    def getWordFrequencyFromStringSentence(self,sentence,wordsCount={}):
        return self.getWordFrequencyFromTokenizedSentence(self.sentenceStringTokenizer(sentence),wordsCount)
    def getWordFrequencyFromTokenizedSentencesList(self,tokenizedSentences,wordsCount={}):
        for sentence in tokenizedSentences:
            wordsCount = self.getWordFrequencyFromTokenizedSentence(sentence,wordsCount)
        return wordsCount
    def getWordFrequencyFromSentencesList(self,sentences,wordsCount={}):
        return self.getWordFrequencyFromTokenizedSentencesList(self.sentenceListTokenizer(sentences),wordsCount)

    def getWordDef(self,word): #gets the definitions of a word
        try: #tries to find the definition
            wordNet = nltk.corpus.wordnet.synsets(word) 
            definitions = [] #if it succeeds it proceeds
            for synset in wordNet:
                definitions.append(synset.definition())
        except: definitions = [word] #if it fails, it returns the word
        if len(definitions) == 0: #ensures that it correctly returns the word back if no definitions were found
            definitions = [word]
        return definitions

    def sentence2wordDict(self,tokenizedSentence,banWords = {}): #Makes a dictionary of word definitions from a sentence 
        sentenceWordDefinitions = {}
        for word in tokenizedSentence:
            if word not in banWords: #updates the dictionary with each new word
                #with the format {word:[definition1,definition2....]}
                sentenceWordDefinitions.update({word:self.getWordDef(word)})
        return sentenceWordDefinitions
    def makeWordDictionary(self,sentencesTokenized):    
    #Make a dictionary for the words
        definitions = {}
        for sentence in sentencesTokenized:
            definitions.update(self.sentence2wordDict(sentence,definitions))
        return definitions

    def gsim_getScore(self,tokenizedSentences):
        score = []
        dictionary = gensim.corpora.Dictionary(tokenizedSentences)
        corpus = [dictionary.doc2bow(sentence) for sentence in tokenizedSentences]
        lsi = gensim.models.LsiModel(corpus, id2word=dictionary, num_topics=2)
        index = gensim.similarities.MatrixSimilarity(lsi[corpus])
        for sentence in tokenizedSentences:
            vec_bow = dictionary.doc2bow(sentence)
            # convert the query to LSI space
            vec_lsi = lsi[vec_bow]
            # perform a similarity query against the corpus
            similarities = index[vec_lsi]
            score.extend(similarities)
        return score

    def similarityPrint(self,arg,result):
        for line in result:
            print(cyan,line[0],reset,">>",blue,line[1],reset)
            for sim in line[2]:
                if sim[0] >= arg.thresholdSim:
                    print(" score:",cyan, "%.2f" % sim[0], reset," >> '",sim[1],"' >> ", blue, sim[2],reset)
            print('\n')

    def similarityExport(self,arg,workbook,result):
        line = 0; col = 0
        worksheet = workbook.add_worksheet("f"+str(arg.iteration)+"-"+arg.mode+'-'+getSimMethods(arg.similarityMethod-1)+"_Similarity")
        worksheet.write(line,col,"Sentence 1")
        worksheet.write(line,col+1,"Sentence 1 tokens")
        worksheet.write(line,col+2,"Top Similarities")
        worksheet.write(line,col+3,"Cross similarity")
        worksheet.write(line,col+4,"Sentence 2")
        worksheet.write(line,col+5,"Sentence 2 tokens")
        leftFormat = workbook.add_format({'align': 'left','valign' : 'top'})
        centFormat = workbook.add_format({'align': 'center','valign' : 'top'})
        ##########################################################################
        #result [   [sentence, tokens,  [similarity, sentence, tokens] ] ,  [sentence, tokens,  [similarity, sentence, tokens] ]]
        line+=1
        for series in result:            
            pivot = 0
            for sim in series[2]:
                if sim[0] >= arg.thresholdSim:
                    worksheet.write(line,col+3,sim[0]) #score
                    worksheet.write(line,col+4,sim[1]) #value
                    worksheet.write(line,col+5,', '.join(sim[2]))  #tokens
                    line += 1; pivot += 1
                else: break
            if pivot == 1:
                worksheet.write(line-pivot,col,series[0]) #value
                worksheet.write(line-pivot,col+1,', '.join(series[1]))  #tokens
                worksheet.write(line-pivot,col+2,pivot,centFormat)
            elif pivot > 1:
                worksheet.merge_range('A'+str(line-pivot+1)+':'+'A'+str(line), series[0],leftFormat) #value
                worksheet.merge_range('B'+str(line-pivot+1)+':'+'B'+str(line),', '.join(series[1]),leftFormat) #tokens
                worksheet.merge_range('C'+str(line-pivot+1)+':'+'C'+str(line), pivot,centFormat) #value
        worksheet.write(0,10,'Threshold value')
        worksheet.write(0,11,arg.thresholdSim)

    def similaritySort(self,arg,rawSentences,tokenizedSentences,score):
        useBreak = True # True - doesn't add scores below the threshold to the results
        if useBreak:
            pos = 0; result = []
            for i in range(0,len(tokenizedSentences)):
                localResult = [] #[score,sentence,tokens]
                for j in range(0,len(tokenizedSentences)):
                    if score[pos] >= arg.thresholdSim:
                        localResult.append([round(score[pos],2),rawSentences[j],tokenizedSentences[j]])
                    pos += 1
                if len(localResult) > 0:
                    localResult = sorted(localResult, key=lambda x: x[0], reverse = True)
                    result.append([rawSentences[i],tokenizedSentences[i],localResult])
        else:
            pos = 0; result = []
            for i in range(0,len(tokenizedSentences)):
                localResult = []
                for j in range(0,len(tokenizedSentences)):
                    localResult.append([score[pos],rawSentences[j],tokenizedSentences[j]])
                    pos += 1
                localResult = sorted(localResult, key=lambda x: x[0], reverse = True)
                result.append([rawSentences[i],tokenizedSentences[i],localResult])
        return result
    # I will generalize those methods if I have time to give more functionality
    # They're meant to calculate similarities from sentences, but I want to take them even further than
    # what they can do right now. I will come back to this... probably
    def gsimSimilarityMethod(self,arg,workbook,rawSentences,stemORlemma='lemma',extraDefinitions ={},banList=[]): 
        # Tokenize the sentences and remove extra data from them
        if stemORlemma == 'lemma':
            tokenizedSentences = self.sentenceListTokenLemmer(self.removeSentencesListStopWords(self.sentenceListTokenizer(rawSentences)))
        elif stemORlemma == 'stem':
            tokenizedSentences = self.sentenceListTokenStemmer(self.removeSentencesListStopWords(self.sentenceListTokenizer(rawSentences)))
        # Build a dictionary from all the sentences
        definitions = self.makeWordDictionary(tokenizedSentences)
        # Add extra synonims for analysis if any are given
        definitions.update(extraDefinitions)
        # Get the score in %
        score = self.gsim_getScore(tokenizedSentences)
        #combine everyhing and sort the similarities
        result = self.similaritySort(arg,rawSentences,tokenizedSentences,score)
        del definitions, score, tokenizedSentences
        # Print results
        if arg.showSimResults:
            self.similarityPrint(arg,result)
        return result
    def tokenDefSimilarity2sentences(self,tokenizedSentence1,tokenizedSentence2,definitions):
        matches = 0
        for word1 in tokenizedSentence1:
            match = 0
            for word2 in tokenizedSentence2:
                for definition1 in definitions[word1]:
                    for definition2 in definitions[word2]:
                        if definition1 == definition2:
                            match = 1
            matches += match
        if len(tokenizedSentence1) > len(tokenizedSentence2):
            similarity = matches / len(tokenizedSentence1)
        else: similarity = matches / len(tokenizedSentence2)
        return similarity
    def tokenSimilarityCalculator(self,tokenizedSentences,definitions):
        #Calculate the number of similar words between sentences
        sentenceMatches = []
        for sentence1 in tokenizedSentences:
            for sentence2 in tokenizedSentences:
                matches = 0
                for word1 in sentence1:
                    match = 0
                    for word2 in sentence2:
                        for definition1 in definitions[word1]:
                            for definition2 in definitions[word2]:
                                if definition1 == definition2:
                                    match = 1
                    matches += match
                sentenceMatches.extend([matches])
        return sentenceMatches

    def tokenSimilarityNormalizer(self,tokenizedSentences,sentenceMatches):
    # Normalize the similarity between sentences
        score = []; pos = 0
        for sentence1 in tokenizedSentences:
            for sentence2 in tokenizedSentences:
                similarity = 0
                if sentenceMatches[pos] != 0:
                    if len(sentence1) <= len(sentence2):
                        similarity = sentenceMatches[pos] / len(sentence2)
                    else:
                        similarity = sentenceMatches[pos] / len(sentence1)
                score.extend([similarity])
                pos += 1
        return score

    def tokenSimilarityMethod(self,arg,sentencesList,extraDefinitions={},banList=[]): 
        # Tokenize the sentences and remove extra data from them
        tokenizedSentences = self.sentenceListTokenLemmer(self.removeSentencesListStopWords(self.sentenceListTokenizer(sentencesList),banList))
        # Build a dictionary from all the sentences
        definitions = self.makeWordDictionary(tokenizedSentences)
        definitions.update(extraDefinitions)
        # Find all the matches from the data
        #print(tokenizedSentences)
        sentencesMatches = self.tokenSimilarityCalculator(tokenizedSentences,definitions)
        # Normalize the score in %
        score = self.tokenSimilarityNormalizer(tokenizedSentences,sentencesMatches)
        result = self.similaritySort(arg,sentencesList,tokenizedSentences,score)
        if arg.showSimResults:
            self.similarityPrint(arg,result)
        return result

    def windowSimilarity_sentenceList(self,tokenizedSentences): # Returns the number of windows matches in a list of lists of sentences
        #Must take as an argument a list of lists of strings
        # Create a simple list to hold each similarity
        windowsMatch = [0 for i in range(0,len(tokenizedSentences)*len(tokenizedSentences))]
        pos = 0
        # Similarity with the window method in a crude and simple way
        for i in range(0,len(tokenizedSentences)):
            for k in range(0,len(tokenizedSentences)):
                match = 0
                for j in range(0, len(tokenizedSentences[i]) -1):
                    w1 = tokenizedSentences[i][j] +' '+ tokenizedSentences[i][j +1]
                    for m in range(0, len(tokenizedSentences[k]) -1):
                        w2 = tokenizedSentences[k][m] +' '+ tokenizedSentences[k][m +1]
                        if w1 == w2:
                            match += 1
                windowsMatch[pos] = match
                pos += 1
        return windowsMatch

    def windowSimilarity_normalizer(self,tokenizedSentences,windowsMatch):
        # Turns the obtained matches from the method sentence_SimilarityWindow
        # into scores for similarity using a custom method
        # Normalize similarity
        score = []; pos = 0
        for i in range(0,len(tokenizedSentences)):
            primaryWindowSize = len(tokenizedSentences[i]) - 1
            for j in range(0,len(tokenizedSentences)):
                secondaryWindoSize = len(tokenizedSentences[j]) - 1
                similarity = 0
                if windowsMatch[pos] != 0:
                    if primaryWindowSize <= secondaryWindoSize:
                        similarity = windowsMatch[pos] / secondaryWindoSize
                    else:
                        similarity = windowsMatch[pos] / primaryWindowSize
                score.extend([similarity])
                pos += 1
        return score

    def windowSmilarityMethod(self,arg,workbook,sentencesList):    
        # Tokenize the sentences and remove extra data from them
        tokenizedSentences = self.sentenceListTokenStemmer(self.sentenceListTokenizer(sentencesList))
        # Find all the matches from the data
        sentencesMatches = self.windowSimilarity_sentenceList(tokenizedSentences)
        # Normalize the score in %
        score = self.windowSimilarity_normalizer(tokenizedSentences,sentencesMatches)
        result = self.similaritySort(arg,sentencesList,tokenizedSentences,score)
        if arg.showSimResults:
            self.similarityPrint(arg,result)
        return result

    def getThr(self,arg,thresholds):
        if len(thresholds) > 1:
            return thresholds[arg.similarityMethod-1]
        else: return thresholds[0]

    def getSimilarity(self,arg,workbook,method,GD,banList=[],extraDefinitions={},stemORlemma='lemma'):
        # this is meant to decide what similarity the program will use based on the arguments
        # on a second though, I will make it so the user decides which method to use
        results = []
        if arg.similarityMethod != None:
            rawSentencesList = self.getSentences(arg,GD)
            rawSentencesList = self.textNormalizer(rawSentencesList)
            thresholds = arg.thresholdSim
            
            if arg.similarityMethod == 1:#gesim
                arg.thresholdSim = self.getThr(arg,thresholds)
                print("Getting similarities with gsim word2vec method, threshold =",arg.thresholdSim)
                results.append(self.gsimSimilarityMethod(arg,workbook,rawSentencesList,stemORlemma,extraDefinitions))
                if arg.exportResults:
                    self.similarityExport(arg,workbook,results[0])
                    print("Similarity data from gsim method exported")

            elif arg.similarityMethod == 2:#token
                arg.thresholdSim = self.getThr(arg,thresholds)
                print("Getting similarities with token method, threshold =",arg.thresholdSim)
                results.append(self.tokenSimilarityMethod(arg,rawSentencesList,extraDefinitions,banList))
                if arg.exportResults:
                    self.similarityExport(arg,workbook,results[0])
                    print("Similarity data from token metod exported")
            
            elif arg.similarityMethod == 3:#window
                arg.thresholdSim = self.getThr(arg,thresholds)
                print("Getting similarities with window method, threshold =",arg.thresholdSim)
                results.append(self.windowSmilarityMethod(arg,workbook,rawSentencesList))
                if arg.exportResults:
                    self.similarityExport(arg,workbook,results[0])
                    print("Similarity data from window method exported")

            elif arg.similarityMethod == 0:
                arg.similarityMethod = 1; arg.thresholdSim = self.getThr(arg,thresholds)
                print("Getting similarities with gsim word2vec method, threshold =",arg.thresholdSim)
                results.append(self.gsimSimilarityMethod(arg,workbook,rawSentencesList,stemORlemma,extraDefinitions))
                if arg.exportResults:
                    self.similarityExport(arg,workbook,results[0])
                    print("Similarity data from gsim method exported")

                arg.similarityMethod = 2; arg.thresholdSim = self.getThr(arg,thresholds)
                print("Getting similarities with token method, threshold =",arg.thresholdSim)
                results.append(self.tokenSimilarityMethod(arg,rawSentencesList,extraDefinitions,banList))
                if arg.exportResults:
                    self.similarityExport(arg,workbook,results[1])
                    print("Similarity data from token metod exported")

                arg.similarityMethod = 3; arg.thresholdSim = self.getThr(arg,thresholds)
                print("Getting similarities with window method, threshold =",arg.thresholdSim)
                results.append(self.windowSmilarityMethod(arg,workbook,rawSentencesList))
                if arg.exportResults:
                    self.similarityExport(arg,workbook,results[2])
                    print("Similarity data from window method exported")
                    
                arg.similarityMethod = 0

            else:
                print("You specified a similarity method not implemented")
            arg.thresholdSim = thresholds
        return results

    def getSentences(self,arg,GD):
        sentences = []
        if arg.mode == 'Q':
            sentences = ['' for i in range(0,len(GD.Questions.byQuestion))]
            for i in range(0,len(GD.Questions.byQuestion)):
                sentences[i] =  GD.Questions.byQuestion[i][1]
        elif arg.mode == 'A':
            sentences = ['' for i in range(0,len(GD.Answers.byAnswer))]
            for i in range(0,len(GD.Answers.byAnswer)):
                sentences[i] =  GD.Answers.byAnswer[i][1]
        elif arg.mode == 'I':
            sentences = []
        return sentences

textProcessing = myLTK()

### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
# * * * * * Functions dealing with soring the data  * * * * * *
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
# if I have time I will make from those 3 functions a general one using dictionaries
# however, lists are probably better in this case given the data
def cleanList(Lst):
    cleanedList = []
    for i in range(0,len(Lst),1):
        local = Lst[i]
        occurences = 1
        if local not in [item[1] for item in cleanedList]:
            for j in range(i+1,len(Lst),1):
                if Lst[i] == Lst[j]:
                    occurences += 1
            cleanedList.append([occurences,local])
    return cleanedList

def sortByImage(data):
    GD = globalHolder() # creates a global holder for the sorted data
    for i in range(0,len(data),1): # goes through each line of the data
        repeats = 1 # initializes a counter to 1 for the occurences of an entry
        LocalImage = data[i][IMG] # takes the image which will be the main key on sorting
        LocalQuestions = [data[i][Q]] # adds the first question occurence for the image
        LocalAnswers = [data[i][A]] # adds the first answer occurence for the image
        # checks to ensure that the key was not already processed
        if LocalImage not in [item[1] for item in GD.Images.byImage]:
            # for speeding up, iterates from the next position after the current one
            # since the ones before were already processed
            for j in range(i+1,len(data),1):
                # checks to see if the keys are the same
                if data[i][IMG] == data[j][IMG]: # if they are
                    repeats += 1 # number of occurences is increased by 1
                    LocalQuestions.append(data[j][Q]) # the question is added to the list of that key
                    LocalAnswers.append(data[j][A]) # the answer is added to the list of that key
            # after the iteration is done, add all the findings to the global lists in the format
            # [occurences, image]
            # [[occurences, QuestionsList], [], ...]
            # [[occurences, AnswersList], [], ...]
            GD.Images.byImage.append([repeats,LocalImage])
            GD.Questions.byImage.append(cleanList(LocalQuestions))
            GD.Answers.byImage.append(cleanList(LocalAnswers))
    return GD

def sortByQuestion(data):
    GD = globalHolder() # creates a global holder for the sorted data
    for i in range(0,len(data),1): # goes through each line of the data
        repeats = 1 # initializes a counter to 1 for the occurences of an entry
        LocalImages = [data[i][IMG]] # adds the first image occurence for the answer
        LocalQuestion = data[i][Q] # takes the question which will be the main key on sorting
        LocalAnswers = [data[i][A]] # the answer is added to the list of that key
        if LocalQuestion not in [item[1] for item in GD.Questions.byQuestion]:
        # for speeding up, iterates from the next position after the current one
        # since the ones before were already processed
            for j in range(i+1,len(data),1):
                 # checks to see if the keys are the same
                if data[i][Q] == data[j][Q]: # if they are
                    repeats += 1  # number of occurences is increased by 1
                    LocalImages.append(data[j][IMG])  # the image is added to the list of that key
                    LocalAnswers.append(data[j][A]) # the answer is added to the list of that key
            # after the iteration is done, add all the findings to the global lists in the format
            # [occurences, Question]
            # [[occurences, ImagesList], [], ...]
            # [[occurences, AnswersList], [], ...]
            GD.Images.byQuestion.append(cleanList(LocalImages))
            GD.Questions.byQuestion.append([repeats,LocalQuestion])
            GD.Answers.byQuestion.append(cleanList(LocalAnswers))
    return GD

def sortByAnswer(data):
    GD = globalHolder() # creates a global holder for the sorted data
    for i in range(0,len(data),1):  # goes through each line of the data
        repeats = 1 # initializes a counter to 1 for the occurences of an entry
        LocalImages = [data[i][IMG]]  # adds the first image occurence for the answer
        LocalQuestions = [data[i][Q]] # adds the first question occurence for the answer
        LocalAnswer = data[i][A]  # takes the answer which will be the main key on sorting
        if LocalAnswer not in [item[1] for item in GD.Answers.byAnswer]:
            # for speeding up, iterates from the next position after the current one
            # since the ones before were already processed
            for j in range(i+1,len(data),1):
                # checks to see if the keys are the same
                if data[i][A] == data[j][A]:# if they are
                    repeats += 1 # number of occurences is increased by 1
                    LocalImages.append(data[j][IMG]) # the image is added to the list of that key
                    LocalQuestions.append(data[j][Q]) # the question is added to the list of that key
            # after the iteration is done, add all the findings to the global lists in the format
            # [occurences, Answer]
            # [[occurences, ImagesList], [], ...]
            # [[occurences, QuestionsList], [], ...]
            GD.Images.byAnswer.append(cleanList(LocalImages))
            GD.Questions.byAnswer.append(cleanList(LocalQuestions))
            GD.Answers.byAnswer.append([repeats,LocalAnswer])
    return GD
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
# * * * * * * * Post processing to get the sizes * * * * * * *
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
# necessary part for obtaining the sorted information
# this is where sizes are calculated
def postProcessSizesByImage(GD):
    GD.Images.sizeByImage = 0
    for i in range(0,len(GD.Images.byImage),1):
        GD.Images.sizeByImage += GD.Images.byImage[i][0]
    for i in range(0,len(GD.Questions.byImage),1):
        GD.Questions.sizeByImage.extend([0])
        for j in range(0,len(GD.Questions.byImage[i]),1):
            GD.Questions.sizeByImage[i] += GD.Questions.byImage[i][j][0]
    for i in range(0,len(GD.Answers.byImage),1):
        GD.Answers.sizeByImage.extend([0])
        for j in range(0,len(GD.Answers.byImage[i]),1):
            GD.Answers.sizeByImage[i] += GD.Answers.byImage[i][j][0]
    return GD
def postProcessSizesByQuestion(GD):
    for i in range(0,len(GD.Images.byQuestion),1):
        GD.Images.sizeByQuestion.extend([0])
        for j in range(0,len(GD.Images.byQuestion[i]),1):
            GD.Images.sizeByQuestion[i] += GD.Images.byQuestion[i][j][0]
    GD.Questions.sizeByQuestion = 0
    for i in range(0,len(GD.Questions.byQuestion),1):
        GD.Questions.sizeByQuestion =  GD.Questions.sizeByQuestion + GD.Questions.byQuestion[i][0]
    for i in range(0,len(GD.Answers.byQuestion),1):
        GD.Answers.sizeByQuestion.extend([0])
        for j in range(0,len(GD.Answers.byQuestion[i]),1):
            GD.Answers.sizeByQuestion[i] += GD.Answers.byQuestion[i][j][0]
    return GD
def postProcessSizesByAnswer(GD):
    for i in range(0,len(GD.Images.byAnswer),1):
        GD.Images.sizeByAnswer.extend([0])
        for j in range(0,len(GD.Images.byAnswer[i]),1):
            GD.Images.sizeByAnswer[i] += GD.Images.byAnswer[i][j][0]
    for i in range(0,len(GD.Questions.byAnswer),1):
        GD.Questions.sizeByAnswer.extend([0])
        for j in range(0,len(GD.Questions.byAnswer[i]),1):
            GD.Questions.sizeByAnswer[i] += GD.Questions.byAnswer[i][j][0]
    GD.Answers.sizeByAnswer =0
    for i in range(0,len(GD.Answers.byAnswer),1):
        GD.Answers.sizeByAnswer += GD.Answers.byAnswer[i][0]
    return GD
def postProcessSizes(GD):
    GD = postProcessSizesByImage(GD)
    GD = postProcessSizesByQuestion(GD)
    GD = postProcessSizesByAnswer(GD)
    return 

### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
# * * * * * * * * * * * Printing functions * * * * * * * * * * 
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
# this is what prints everything in the terminal for the first part of the analysis
def printByImage(GD,argm):
     print("Different Images:",len(GD.Images.byImage))
     for i in range(0,len(GD.Images.byImage),1):
        print(cyan, "Image: ", reset, "Times: ", GD.Images.byImage[i][0])
        if argm.showImages:
            print(GD.Images.byImage[i][1])
        print(cyan, "Questions: ", reset, GD.Questions.sizeByImage[i])
        if argm.showQuestions:
            for j in range(0,len(GD.Questions.byImage[i])):
                print(GD.Questions.byImage[i][j])
        print(cyan, "Answers: ", reset, GD.Answers.sizeByImage[i])
        if argm.showAnswers:
            for j in range(0,len(GD.Answers.byImage[i]),1):
                print(GD.Answers.byImage[i][j])
        print("\n")
def printByQuestion(arg,GD):
    print("Different questions:",len(GD.Questions.byQuestion))
    for i in range(0,len(GD.Questions.byQuestion),1):
        print(cyan ,"Question:", reset, "Times: ",GD.Questions.byQuestion[i][0])
        if arg.showQuestions:
            print("Q: ", GD.Questions.byQuestion[i][1])
        print(cyan, "Answers: ", reset, GD.Answers.sizeByQuestion[i])
        if arg.showAnswers:
            for j in range(0,len(GD.Answers.byQuestion[i]),1):
                print(GD.Answers.byQuestion[i][j])
        print(cyan, "Images: ", reset, GD.Images.sizeByQuestion[i]) #, gImages.byQuestion[i])
        if arg.showImages:
            for j in range(0,len(GD.Images.byQuestion[i]),1):
                print(GD.Images.byQuestion[i][j])
        print("\n")
def printByAnswer(arg,GD): # I didn't test this in a long time since it's abandoned. Probably still works
    print("Different Answers:",len(GD.Answers.byAnswer))
    for i in range(0,len(GD.Answers.byAnswer),1):
        print(cyan, "Answer: ", reset, "Times: ", GD.Answers.byAnswer[i][0])
        if arg.showAnswers:
            #for j in range(0,len(gAnswers.byAnswer[i]),1):
            print(GD.Answers.byAnswer[i][1])
        print(cyan ,"Questions:", reset, GD.Questions.sizeByAnswer[i])
        if arg.showQuestions:
            for j in range(0,len(GD.Questions.byAnswer[i])):
                print(GD.Questions.byAnswer[i][j])
        print(cyan, "Images: ", reset, GD.Images.sizeByAnswer[i])
        if arg.showImages:
            for j in range(0,len(GD.Images.byAnswer[i]),1):
                print(GD.Images.byAnswer[i][j])
        print("\n")
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
# * * * * * * * * * * * Exporting functions * * * * * * * * * * 
### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ###
def getThresholdQA(arg,allX):
    #allX [     [feqP, P,   [   [freqC,C], [freqC,C]    ]   ] , [feqC, C,   [   [freqC,C], [freqC,C]    ]   ] ]
    if arg.useMedian: #Calculates the median of the entire data
        #[word for sentence in text for word in sentence]
        medianList = [item[0] for item in allX]
        thresholdParent = statistics.median(medianList)
        medianList = [item[0] for x in allX for item in x[2]]
        thresholdChild = statistics.median(medianList)
    else: #sets the designated threshold value
        thresholdParent = arg.thresholdParent
        thresholdChild = arg.thresholdChild
    return thresholdParent, thresholdChild
def getSortedQuestionsWitthAnswers(GD):
    allQuestions = []
    for i in range(0,len(GD.Questions.byQuestion),1):
        A41Q = []
        for j in range(0,len(GD.Answers.byQuestion[i]),1):
            A41Q.append([GD.Answers.byQuestion[i][j][0],GD.Answers.byQuestion[i][j][1]])
        A41Q = sorted(A41Q, key=lambda x: x[0], reverse = True)
        #A41Q [     [freqA,A], [freqA,A]     ]
        allQuestions.append([GD.Questions.byQuestion[i][0],GD.Questions.byQuestion[i][1],A41Q])
    allQuestions = sorted(allQuestions, key=lambda x: x[0], reverse = True)    
    #AllQ [     [feqQ, Q,   [   [freqA,A], [freqA,A]    ]   ] , [feqQ, Q,   [   [freqA,A], [freqA,A]    ]   ] ]
    return allQuestions
def getSortedAnswersWitthQuestions(GD):
    allAnswers = []
    for i in range(0,len(GD.Answers.byAnswer),1):
        Q41A = []
        for j in range(0,len(GD.Questions.byAnswer[i]),1):
            Q41A.append([GD.Questions.byAnswer[i][j][0],GD.Questions.byAnswer[i][j][1]])
        Q41A = sorted(Q41A, key=lambda x: x[0], reverse = True)
        #Q41A [     [freqQ,Q], [freqQ,Q]     ]
        allAnswers.append([GD.Answers.byAnswer[i][0],GD.Answers.byAnswer[i][1],Q41A])
    allAnswers = sorted(allAnswers, key=lambda x: x[0], reverse = True)
    return allAnswers

def export2xlsQA(worksheet,centFormat,leftFormat,allX,thresholdParent,thresholdChild,line,col):
    # #AllX [     [feqP, P,   [   [freqC,C], [freqC,C]    ]   ] , [feqP, P,   [   [freqC,C], [freqC,C]    ]   ] ]
    otherPairs = []
    for Pair in allX:
        if Pair[0] > thresholdParent:
            #get the threshold for the answers on which to split data
            pivot = 0; others = [] #sets a pivot value for remembering how many lines were written
                #for one question to know how far to merge and to jump when done
                #others keeps the other values (combined) that do not meet the required threshold
            for QACouple in Pair[2]:
                if QACouple[0] >= thresholdChild: #if value above or equal the median
                    #Add the frequency of the answer on col D
                    worksheet.write(line,col+3,QACouple[0])
                    #Add the answer on col E
                    worksheet.write(line,col+4,QACouple[1])
                    line += 1; pivot += 1
                else: others.append(QACouple) #if not, add it to the others section
            if len(others) >= 1: #if there's at least 1 element in the others section
                frequencyC = 0; valueC = "others: " #starts adding together all the other values
                for other in others:
                    frequencyC += other[0]
                    valueC += (", " + other[1])
                #Add the frequency of the answer/question on col D
                worksheet.write(line,col+3,frequencyC)
                #Add the answer/question on col E
                worksheet.write(line,col+4,valueC)
                line += 1; pivot += 1
            if len(Pair[2]) == 1 or pivot == 1:
                #Add frequency of question/answer on col A and merge all cells below until next entry
                worksheet.write(line -pivot, col, Pair[0])
                #Add the question/answer on col A and merge all cells below until next entry
                worksheet.write(line -pivot, col + 1, Pair[1])
                #Add the number of unique answers/questions for the question/answer to the col C
                worksheet.write(line -pivot,col+2,len(Pair[2]))
            else:
                #Add frequency of question on col A and merge all cells below until next entry
                #len(gAnswers.byQuestion[i]) - number of answers for Qi
                worksheet.merge_range('A'+str(line-pivot+1)+':'+'A'+str(line), Pair[0], centFormat)
                #Add the question/answer on col A and merge all cells below until next entry
                worksheet.merge_range('B'+str(line-pivot+1)+':'+'B'+str(line), Pair[1], leftFormat)
                #Add the number of unique answers/questions for the question/answers to the col C
                worksheet.merge_range('C'+str(line-pivot+1)+':'+'C'+str(line), len(Pair[2]), centFormat)
        else:
            otherPairs.append(Pair) # everything that's not meeting the threshold constraint is going here
    # if there are any that didn't meet the threshold constraint will be processed in the next part
    if len(otherPairs) > 0: 
        frequencyP = 0; valueP = "other: "; allC = {}; frequencyC = 0; valueC = "other: "
        # put everything together
        for item in otherPairs:
            frequencyP += item[0]
            valueP += (", "+item[1])
            for subC in item[2]:
                allC[subC[1]] = subC[1]
                frequencyC += subC[0]
                valueC += (", "+subC[1])
        # export the other values
        worksheet.write(line,col,frequencyP,centFormat)
        worksheet.write(line,col+1,valueP,leftFormat)
        worksheet.write(line,col+2,len(allC),centFormat)
        worksheet.write(line,col+3,frequencyC,centFormat)
        worksheet.write(line,col+4,valueC,centFormat)
    # mention in the file the thresholds
    worksheet.write(0,11,'Threshold values')
    worksheet.write(1,10,'Parent threshold')
    worksheet.write(1,11,thresholdParent)
    worksheet.write(2,10,'Child threshold')
    worksheet.write(2,11,thresholdChild)
    ''' more advanced version, will add later a mode for it... probably
    if len(otherPairs) > 0:
        frequencyP = 0; valueP = "other: "; childP = {}
        for item in otherPairs:
            frequencyP += item[0]
            valueP += (", "+item[1])
            for subC in item[2]:
                if subC[1] in childP:
                    childP[subC[1]] += subC[0]
                else: childP[subC[1]] = subC[0]
        worksheet.write(line,col,frequencyP,centFormat)
        worksheet.write(line,col+1,valueP,leftFormat)
        worksheet.write(line,col+3,len(childP),centFormat)
        childP = sorted(childP.items(), key=lambda kv: kv[1],reverse = True)
        frequencyC = 0; valueC = "other: "
        for item in childP:
            if item[1] > thresholdChild:
                worksheet.write(line,col+4,item[1],centFormat)
                worksheet.write(line,col+5,item[0],centFormat)
                line+=1
            else:
                frequencyC += item[1]
                valueC += (", " + item[0])
        worksheet.write(line,col+4,frequencyC,centFormat)
        worksheet.write(line,col+5,valueC,centFormat)
    '''
def exportByQuestion(arg,workbook,GD):
    col = 0; line = 0
    #Add a new sheet to the xls file
    worksheet = workbook.add_worksheet("f"+str(arg.iteration)+"-Questions") 
    #Add labels on the first line 0 = 1
    worksheet.write(line, col, "Question freq.")
    worksheet.write(line, col+1, "Question")
    worksheet.write(line, col+2, "Unique Answers for the question")
    worksheet.write(line, col+3, "Answer freq.")
    worksheet.write(line, col+4, "Answer")
    #Set the merging formats
    centFormat = workbook.add_format({'align': 'center', 'valign' : 'top'})
    leftFormat = workbook.add_format({'align': 'left','valign' : 'top'})
    line +=1
    allQ = getSortedQuestionsWitthAnswers(GD)
    #AllQ [     [feqQ, Q,   [   [freqA,A], [freqA,A]    ]   ] , [feqQ, Q,   [   [freqA,A], [freqA,A]    ]   ] ]
    thresholdParent, thresholdChild = getThresholdQA(arg,allQ)
    export2xlsQA(worksheet,centFormat,leftFormat,allQ,thresholdParent,thresholdChild,line,col)
def exportByAnswer(arg,workbook,GD):
    col = 0; line = 0; allA = []
    #Add a new sheet to the xls file
    worksheet = workbook.add_worksheet("f"+str(arg.iteration)+"-Answers") 
    #Add labels on the first line 0 = 1
    worksheet.write(line, col, "Answer freq.")
    worksheet.write(line, col+1, "Answer")
    worksheet.write(line, col+2, "Unique questions")
    worksheet.write(line, col+3, "Question freq.")
    worksheet.write(line, col+4, "Question")
    #Set the merging formats
    centFormat = workbook.add_format({'align': 'center', 'valign' : 'top'})
    leftFormat = workbook.add_format({'align': 'left','valign' : 'top'})
    line +=1
    for i in range(0,len(GD.Answers.byAnswer),1):
        Q41A = []
        for j in range(0,len(GD.Questions.byAnswer[i]),1):
            Q41A.append([GD.Questions.byAnswer[i][j][0],GD.Questions.byAnswer[i][j][1]])
        Q41A = sorted(Q41A, key=lambda x: x[0], reverse = True)
        #Q41A [     [freqQ,Q], [freqQ,Q]     ]
        allA.append([GD.Answers.byAnswer[i][0],GD.Answers.byAnswer[i][1],Q41A])
    allA = sorted(allA, key=lambda x: x[0], reverse = True)    
    #allA [     [feqA, A,   [   [freqQ,Q], [freqQ,Q]    ]   ] , [feqQ, Q,   [   [freqQ,Q], [freqQ,Q]    ]   ] ]
    thresholdParent, thresholdChild = getThresholdQA(arg,allA)
    export2xlsQA(worksheet,centFormat,leftFormat,allA,thresholdParent,thresholdChild,line,col)
def exportWordQuestionCount(arg,stemORlemma,banList,workbook,workbookName,GD):
    mNLTK = myLTK(); line = 0; col = 0; medianList = []
    #create a list holder for the sentences
    sentences =  ['' for i in range(0,len(GD.Questions.byQuestion))]
    #fill the list holder with sentences
    for i in range(0,len(GD.Questions.byQuestion)):
            sentences[i] =  GD.Questions.byQuestion[i][1]
    #remove special characters from each sentence from the list
    sentences =  mNLTK.textNormalizer(sentences)
    #tokenize each sentence from the list
    sentences = mNLTK.sentenceListTokenizer(sentences)
    #remove stop words from the list of sentences
    sentences = mNLTK.removeSentencesListStopWords(sentences, banList)
    # stem or lemmatize each word from the list of sentences
    if stemORlemma == 'stem':
        sentences = mNLTK.sentenceListTokenStemmer(sentences)
    elif stemORlemma == 'lemma':
        sentences =  mNLTK.sentenceListTokenLemmer(sentences,arg.lemmatizer)
    #create a dictionary holder for each word in the sentence
    wordDict = dict()
    #go through the list of sentence and add each word in the dictionary
    for sentence in range(0,len(sentences)):
        for word in sentences[sentence]:
            if word in wordDict: #if the word is already added then
                #add the word ensuring the count is correct in relation to the question frequency
                wordDict[word] += 1 * GD.Questions.byQuestion[sentence][0]
            else: #if the word is new add it to the dictionary
                wordDict[word] = 1 * GD.Questions.byQuestion[sentence][0]
    #sort the dictionary by the count of words
    wordDict = sorted(wordDict.items(), key=lambda kv: kv[1],reverse = True)
    # Put everything in xls
    worksheet = workbook.add_worksheet(workbookName) 
    worksheet.write(line, col, "Word")
    worksheet.write(line, col+1, "Occurences")
    if arg.useMedian: #Calculates the median of the entire data
        for w in wordDict:
            medianList.extend([w[1]])
        threshold = statistics.median(medianList)
    else: #sets the designated threshold value
        threshold = arg.thresholdWC
    #make a holder for the other data that doesn't meet the threshold
    others = []
    for item in wordDict: #iterate through data and add it to xls
        if item[1] >= threshold: #if data is compying with the threshold add it directly
            line += 1
            worksheet.write(line, col, item[0])
            worksheet.write(line, col+1, item[1])
        else: #if data is not coplying with the threshold add it to the others
            others.append([item[0],item[1]])
    if len(others) > 0: #if there is at least one item that didn't comply with the threshold
        s= 0; ans = "others: " #add everything up in a single cell
        for other in others:
            s += other[1]
            ans += (", "+other[0])
        line += 1
        worksheet.write(line, col, ans)
        worksheet.write(line, col+1, s)
    worksheet.write(0,10,'Word count threshold')
    worksheet.write(0,11,threshold)
def exportWordAnswerCount(arg,stemORlemma,banList,workbook,workbookName,GD):
    mNLTK = myLTK(); line = 0; col = 0; medianList = []
    #create a list holder for the sentences
    sentences =  ['' for i in range(0,len(GD.Answers.byAnswer))]
    #fill the list holder with sentences
    for i in range(0,len(GD.Answers.byAnswer)):
            sentences[i] =  GD.Answers.byAnswer[i][1]
    #remove special characters from each sentence from the list
    sentences =  mNLTK.textNormalizer(sentences)
    #tokenize each sentence from the list
    sentences = mNLTK.sentenceListTokenizer(sentences)
    #remove stop words from the list of sentences
    sentences = mNLTK.removeSentencesListStopWords(sentences, banList)
    # stem or lemmatize each word from the list of sentences
    if stemORlemma == 'stem':
        sentences = mNLTK.sentenceListTokenStemmer(sentences)
    elif stemORlemma == 'lemma':
        sentences =  mNLTK.sentenceListTokenLemmer(sentences,arg.lemmatizer)
    #create a dictionary holder for each word in the sentence
    wordDict = dict()
    #go through the list of sentence and add each word in the dictionary
    for sentence in range(0,len(sentences)):
        for word in sentences[sentence]:
            if word in wordDict: #if the word is already added then
                #add the word ensuring the count is correct in relation to the question frequency
                wordDict[word] += 1 * GD.Answers.byAnswer[sentence][0]
            else: #if the word is new add it to the dictionary
                wordDict[word] = 1 * GD.Answers.byAnswer[sentence][0]
    #sort the dictionary by the count of words
    wordDict = sorted(wordDict.items(), key=lambda kv: kv[1],reverse = True)
    # Put everything in xls
    worksheet = workbook.add_worksheet(workbookName) 
    worksheet.write(line, col, "Word")
    worksheet.write(line, col+1, "Occurences")
    if arg.useMedian: #Calculates the median of the entire data
        for w in wordDict:
            medianList.extend([w[1]])
        threshold = statistics.median(medianList)
    else: #sets the designated threshold value
        threshold = arg.thresholdWC
    #make a holder for the other data that doesn't meet the threshold
    others = []
    for item in wordDict: #iterate through data and add it to xls
        if item[1] >= threshold: #if data is compying with the threshold add it directly
            line += 1
            worksheet.write(line, col, item[0])
            worksheet.write(line, col+1, item[1])
        else: #if data is not coplying with the threshold add it to the others
            others.append([item[0],item[1]])
    if len(others) > 0: #if there is at least one item that didn't comply with the threshold
        s= 0; ans = "others: " #add everything up in a single cell
        for other in others:
            s += other[1]
            ans += (", "+other[0])
        line += 1
        worksheet.write(line, col, ans)
        worksheet.write(line, col+1, s)
    worksheet.write(0,10,'Word count threshold')
    worksheet.write(0,11,threshold)

def getDataIndexes(sentences):
   #data selecting
    DataDict ={}
    for i in range(0,len(sentences)):
        for scan in range(i,len(sentences)):
            if sentences[i] == sentences[scan]:
                if sentences[scan] in DataDict:
                    DataDict[sentences[scan]].extend([scan])
                else: DataDict[sentences[scan]] = [scan]
    #data migrating to list with indexes
    DataList = []
    for item in DataDict:
        DataList.append([item,DataDict[item]])
    #DataList [sentence,[indx,indx]], [...]
    return DataList
def dataMerge(DataList):
    # merging identical entries
    for i in range(0,len(DataList)):
        indx = []
        for j in range(i+1,len(DataList)):
            if DataList[i][0] == DataList[j][0]:
                indx.extend([j])
        err = 0
        for j in indx:
            DataList[i][1].extend(DataList[j-err][1])
            del DataList[j-err]
            err+=1
    return DataList
def buildDefinitions(DataList):
    #making word definitions
    definitions = {}
    for pair in DataList:
        for token in pair[0]:
            normal = textProcessing.getWordDef(token)
            stemmed = textProcessing.getWordDef(textProcessing.wordStemmer(token))
            lemmed = textProcessing.getWordDef(textProcessing.wordLemmer(token))
            alldef = list(set().union(normal, stemmed, lemmed))
            definitions[token] = alldef
            definitions[textProcessing.wordStemmer(token)] = alldef
            definitions[textProcessing.wordLemmer(token)] = alldef
    ''' old version
    definitionsList = []
    for word in definitions:
        definitionsList.append([word, definitions[word]])
    del definitions    
    for i in range(0,len(definitionsList)):
        definitionsList[i][0] = textProcessing.wordStemmer(definitionsList[i][0]) #Uses stemmer
        for defin in textProcessing.getWordDef(definitionsList[i][0]):
            if defin not in definitionsList[i][1]:
                definitionsList[i][1].extend([defin])
    definitions = {}
    for pair in definitionsList:
        definitions[pair[0]] = pair[1]
    del definitionsList
    '''
    return definitions
def mergeByDictionary(DataList,definitions):
    coef = 0.9
    # merging identical entries
    for i in range(0,len(DataList)):
        indx = []
        for j in range(i+1,len(DataList)):
            match = 0
            for token1 in DataList[i][0]:
                for token2 in DataList[j][0]:
                    add = 0
                    for def1 in definitions[token1]:
                        for def2 in definitions[token2]:
                            if def1 == def2:
                                add=1
                    match += add
            if match >= (len(DataList[i][0])+len(DataList[j][0])/2)*coef:
                indx.extend([j])
                #print(DataList[i][0])
                #print(DataList[j][0])
        err = 0
        for j in indx:
            DataList[i][1].extend(DataList[j-err][1])
            del DataList[j-err]
            err+=1
    return DataList
def mergeEverything(sentences,rmsw=False,safety = False):
    DataList = getDataIndexes(sentences) #each sentence with its indexes in the original data
    #tokenizing
    if rmsw:
        for i in range(0,len(DataList)):
            DataList[i][0] = textProcessing.removeTokenStopWords(textProcessing.sentenceStringTokenizer(textProcessing.stringNormalizer(DataList[i][0])),safety= safety)
    else: 
        for i in range(0,len(DataList)):
            DataList[i][0] = textProcessing.sentenceStringTokenizer(textProcessing.stringNormalizer(DataList[i][0]))
   
    #DataList [ [   [token,token,token],[indx,indx] ],  [...]   ]
    ''' guess that's not what the teacher wanted
    #Merge sentences that are exactly the same
    DataList = dataMerge(DataList)
    # build a dictionary of definitions
    definitions = buildDefinitions(DataList)
    # merge all items that are similar in definition
    DataList = mergeByDictionary(DataList,definitions)
    '''
    return DataList

def exportProcessedData(arg):
    #data loading
    data = loadData(arg.files)
    questions = []
    # get all the questions
    for line in data:
        questions.append(line[1])
    DataList = mergeEverything(questions,rmsw = arg.removeStopWords,safety = arg.processFileSafety)
    del questions
    for pair in DataList:
        for indx in pair[1]:
            data[indx][1] = pair[0]
    del DataList
    answers = []
    # get all the answers
    for line in data:
        answers.append(line[2])
    DataList = mergeEverything(answers,rmsw = arg.removeStopWords,safety = arg.processFileSafety)
    del answers
    for pair in DataList:
        for indx in pair[1]:
            data[indx][2] = pair[0]
    del DataList
    for i in range(0,len(data)): # not sure why I put 2 for ... will probably check later to see if I was too tired to think properly
        for j in range(0,len(data[i][1])):
            if arg.processFileMode == 'stem': data[i][1][j] = textProcessing.wordStemmer(data[i][1][j])
            elif arg.processFileMode == 'lem': data[i][1][j] = textProcessing.wordLemmer(data[i][1][j])
            else: break
        for j in range(0,len(data[i][2])):
            if arg.processFileMode == 'stem': data[i][2][j] = textProcessing.wordStemmer(data[i][2][j])
            elif arg.processFileMode == 'lem': data[i][2][j] = textProcessing.wordLemmer(data[i][2][j])
            else: break
    data = sorted(data, key=lambda x: x[0], reverse = False)
    #export data
    data2file(arg,data)
    return data
def data2file(arg,data):
    output = open(arg.processFileOutput, 'w')
    for line in data:
        output.write(line[0]+"|"+" ".join(line[1])+"|"+" ".join(line[2])+os.linesep)  # python will convert \n to os.linesep
    output.close()  # you can omit in most cases as the destructor will call it

'''
def getWordFrequencyFromTokenizedSentence(tokenizedSentence,definitions={}):
    # count how many similar words there are in a tokenized sentence
    matches = 0
    for word1 in tokenizedSentence:
        match = 0
        for word2 in tokenizedSentence:
            if len(definitions) > 0:
                for def1 in definitions[word1]:
                    for def2 in definitions[word2]:
                        if def1 == def2:
                            match = 1
            else:
                if word1 == word2:
                    match = 1
        matches += match
    return matches
def getWordFrequencyFromSentence(sentence,definitions={}):
    # count how many similar words there are in a string sentence
    getWordFrequencyFromTokenizedSentence
def getWordFrequencyFromTokenizedSentencesList():
    None
def getWordFrequencyFromSentencesList():
    None
'''

def wordDenseRepresentation(arg):
    modelPath = arg.wordRepresentationModel
    indx = 0 if arg.mode == "I" else 1 if arg.mode == "Q" else 2 if arg.mode == "A" else 4
    # load data from file
    data = loadData(arg.files)
    sentences = []
    # put each sentence in a list
    for line in data:
        sentences.append(line[indx])
    # make a list of lists of tokens from each sentence
    sentences = textProcessing.removeSentencesListStopWords((textProcessing.sentenceListTokenizer((textProcessing.textNormalizer(sentences)))))
    wordCount = textProcessing.getWordFrequencyFromTokenizedSentencesList(sentences)
    # if there are no specific models to be used
    # or if is of importance only the similarity within the text
    
    if arg.wordRepresentationModel == None:
        print("Training the data model")
        modelName = 'newMSTMmodel.txt'
        # train a model with the current data
        model = gensim.models.Word2Vec(sentences,size=arg.wordRepresentationVectorSize, window=arg.wordRepresentationWindowSize, min_count=arg.wordRepresentationMinCount,workers=arg.wordRepresentationCores, sg=arg.wordRepresentationSG)
        # get existend words from the vocabulary and put them in a list
        words = list(model.vocab)
        #print(words)
        # save model
        model.save_word2vec_format(modelName,binary=True)
        # set the model to the saved one
        if arg.wordRepresentationModel == None: arg.wordRepresentationModel = modelName
        # if there is an already trained model, then it can be used instead
    else:
        # speed up the process and not train a new model
        words = []
        # get the word list
        for sentence in sentences:
            for word in sentence:
                if word not in words: words.extend([word])
    
    print("Loading the trained model", arg.wordRepresentationModel)
    # I used pubmed trained model with a window of 5 and skip-gram
    model = gensim.models.Word2Vec.load_word2vec_format(arg.wordRepresentationModel,binary=True)
    print("Data model succesfully loaded")
    arg.wordDenseRepresentation = modelPath
    vectors=[]; newWords = []; dataVocab = list(model.vocab)
    #print(dataVocab)
    for word in words:
        if word in dataVocab:
            if wordCount[word] >= arg.wordRepresentationMinCount and len(word)>=arg.wordRepresentationMinLength:
                vectors.append(model[word])
                newWords.extend([word])
            #else: print("Attention! '"+word+"' was ommited")
        else: print("Warning! '"+word+"' not in the trained model.")    
    # Principal component analysis
    pca = PCA(n_components=2)
    result = pca.fit_transform(vectors)
    # create a scatter plot of the projection
    print("Plotting the graph.")
    pyplot.scatter(result[:, 0], result[:, 1])
    for i, word in enumerate(newWords):
        pyplot.annotate(word, xy=(result[i, 0], result[i, 1]))
    print("Displaying the graph")
    pyplot.show()
    print()