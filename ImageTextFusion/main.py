#
# * This progam uses files under the format:
# * [image]|[question]|[answer]
# * The results of the program are the analysis of the
# * data from 3 perspectives, namely the image, question
# * and answer perspective. One perspective show how many
# * times the item appears in the data and what items are
# * releated to it, including their occurences
#
import myParser,setup,sys, logging
#import libMST as MST
def start(arg):
    arg, ready = setup.begin(arg)
    if ready:
        if setup.checkFiles(arg):
            try:
                print("\033[90m",end='',flush=True)
                import program as PM
                print("\033[0m")
                print("Library succesfully imported")
            except Exception as e:
                logging.error("Error importing libraries."+ str(e))
                sys.exit(1)
            for mode in arg.mode:
                if mode not in PM.MST.getModes():
                    print("Error! You have specified an invalid mode.")
                    sys.exit(1)
            PM.start(arg)
        else:
            print("No files were loaded.")
            sys.exit(1)      
    else:
        print("Could not start the program")
        sys.exit(1)
def main():
    parser = myParser.myParse()
    args = parser.parse_args()
    if len(sys.argv) > 1:
        if args.verbose:
            myParser.help()
        else: start(args)
    else:
        myParser.help()
if __name__ == "__main__":
    main()
