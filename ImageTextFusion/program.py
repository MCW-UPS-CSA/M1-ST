import libMST as MST
import xlsxwriter as xls
programModes = ['sort','sim','represent']
def recursive(arg,workbook=[]):
    for i in range(0,len(arg.origFiles)):
        arg.iteration = i
        arg.files = [arg.origFiles[i]]
        cycle(arg,workbook)

def cycle(arg,workbook=[]):
    modes = MST.getModes()
    userModes = arg.mode
    for userM in userModes:
        for sysM in modes:
            if userM == sysM:
                arg.mode = userM
                run(arg,workbook)
    arg.mode = userModes
def start(arg):
    arg.origFiles = arg.files
    if arg.exportResults:
        arg.workbook = xls.Workbook('output.xlsx')
    if arg.recursive:
        recursive(arg,arg.workbook)
        # for the merging of all data used in the recursive execution
        if arg.recursiveMerge:
            arg.files = arg.origFiles
            arg.iteration = 'all'
            cycle(arg,arg.workbook)
    else:
        cycle(arg,arg.workbook)
    if arg.exportResults:
        worksheet = arg.workbook.add_worksheet("files")
        for i in range(0,len(arg.origFiles)):
            worksheet.write(i,0,i)
            worksheet.write(i,1,arg.origFiles[i].name)
        arg.workbook.close()

def run(arg,workbook):
    print("Program started running in mode:",arg.mode)
    # Load raw data from files
    data = MST.loadData(arg.files)
    print("Number of lines loaded from files: ",len(data))
    print("\nFiles executed now:")
    print(arg.files,'\n')
    
    if arg.analyseData:
        #Create the sorted data containers
        GD = MST.globalHolder()
        
        #Arrange data depending on the selected mode
        if arg.mode == 'I':
            GD = MST.sortByAnswer(data)
            GD = MST.postProcessSizesByImage(GD)
            print("Attention! You have specified an abandoned module!")
            print("Due to the lack of interest this module received it was not developed further")
            print("It is still possible that it might work correctly by reusing some functions from the other modes")
        elif arg.mode == 'Q':
            GD = MST.sortByQuestion(data)
            print("Q data sorted")
            GD = MST.postProcessSizesByQuestion(GD)
            print("Q data processed")
            if arg.showResults:
                MST.printByQuestion(arg,GD)
                print("Q data printed")
            if arg.exportResults:
                MST.exportByQuestion(arg,workbook,GD)
                print("Q data exported")
                MST.exportWordQuestionCount(arg,'stem',[],workbook,'f'+str(arg.iteration)+'-Q_Stem',GD)
                print("Q word count - stem exported")
                MST.exportWordQuestionCount(arg,'lemma',[],workbook,'f'+str(arg.iteration)+'-Q_Lemma',GD)
                print("Q word count - lemma exported")
        elif arg.mode == 'A':
            GD = MST.sortByAnswer(data)
            print("A data sorted")
            GD = MST.postProcessSizesByAnswer(GD)
            print("A data processed")
            if arg.showResults:
                MST.printByAnswer(arg,GD)
                print("A data printed")
            if arg.exportResults:
                MST.exportByAnswer(arg,workbook,GD)
                print("A data exported")
                MST.exportWordAnswerCount(arg,'stem',[],workbook,'f'+str(arg.iteration)+'-A_Stem',GD)
                print("A word count - stem exported")
                MST.exportWordAnswerCount(arg,'lemma',[],workbook,'f'+str(arg.iteration)+'-A_Lemma',GD)
                print("A word count - lemma exported")  
        ##################################################################################
        #SIMILARITY
        if arg.similarityMethod != None:    
            # get all the extra definitions that you want to add to the program
            # eventually this could be loaded from a specified file
            # maybe done in future updates
            extraDefinitions = {"kind":["type"],"scan":["image"],"contrast":["noncontrast"],"t1":["t2","t3","t4","flair"],
                    "t2":["t1","t3","t4","flair"],"t3":["t1","t2","t4","flair"],"t4":["t2","t3","t1","flair"],"flair":["t2","t3","t1","t4"],
                    "captured":["taken"]
                }
            banList = []
            arg.banList = []
            arg.extraDefinitions = {}
            MST.textProcessing.getSimilarity(arg,workbook,arg.similarityMethod,GD,arg.banList,arg.extraDefinitions)        

            '''
            if arg.similarityMethod != None and (arg.mode == 'Q' or arg.mode == 'A' ):
                if arg.similarityMethod == 0: #gsim seems useless in this case for the data
                    arg.similarityMethod = 2
                    MST.textProcessing.getSimilarity(arg,workbook,arg.similarityMethod,GD)
                    arg.similarityMethod = 3
                    MST.textProcessing.getSimilarity(arg,workbook,arg.similarityMethod,GD)
                    arg.similarityMethod = 0
                else: MST.textProcessing.getSimilarity(arg,workbook,arg.similarityMethod,GD)
            '''

            print("Similarity data processed")
    
    if arg.processData:
        if arg.processFile:
            MST.exportProcessedData(arg)
        if arg.wordRepresentation:
            MST.wordDenseRepresentation(arg)
    
    print(MST.grey,"Program execution completed for:")
    print(arg.files,'\n\n',MST.reset)
    