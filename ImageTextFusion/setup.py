import sys,types,pathlib
def begin(arg):
    ''' not enough time to implement this
    if arg.openFigure and (arg.processData or arg.analyseData):
        print("Error! You can't process or analyse AND see a figure at the same time")
        return arg, False 
    if arg.openFigure:
        if arg.files == None:
            print("Error! You did not specify any file to open.")
            return arg, False 
        elif len(arg.files) > 1:
            print("Error! You have specified too many files to open at the same time.")
            return arg, False
        if arg.files.name.startswith('~/'):
            arg.files = arg.files.expanduser()
        else:
            arg.files = arg.files.resolve()
    else:
    '''

    # create a variable called 'iteration' in the arg namespace
    arg.iteration = 0
    arg.workbook = []
    # deal with threshold values conflicts
    if len(arg.thresholdSim) != 1 and len(arg.thresholdSim) != 3:
        print("Error! You have specified an invalid number of thresholds for the similarity.")
        return arg, False 
    if len(arg.thresholdSim) >1 and arg.similarityMethod != 0:
        print("Error! You have specified too many thresholds for one similarity method.")
        return arg, False
    if arg.useMedian == True and (arg.thresholdParent != 0 or arg.thresholdChild != 0 or arg.thresholdWC != 0 or arg.threshold != None) :
        print("Error! You have designated a threshold value and specified to use the median at the same time.")
        return arg, False 
    if arg.threshold != None and (arg.thresholdParent != 0 or arg.thresholdChild != 0 or arg.thresholdWC != 0):
        print("Error! You have designated a threshold in two different ways.")
        return arg, False
    if arg.threshold != None and (arg.thresholdParent == 0 or arg.thresholdChild == 0 or arg.thresholdWC == 0 or arg.useMedian == False):
        arg.thresholdParent = int(arg.threshold[0])
        arg.thresholdChild = int(arg.threshold[1])
        arg.thresholdWC = int(arg.threshold[2])

    # dealing with path and files conflicts
    if arg.path == pathlib.Path.cwd() and arg.files == None and arg.fileExtensions == None:
        print("Error! You must specify at least one path, or one file, or one extension")
        return arg, False

    # dealing with conflicts when a path is specified
    if arg.path != None:
        # get current path absolute path
        currentPath = pathlib.Path.cwd()
        # verify the paths to be correct
        for p in range(0,len(arg.path)):
            # get the absolute paths from the input if they're not absolute
            if arg.path[p].name.startswith('~/'):
                arg.path[p] = arg.path[p].expanduser()
            else:
                arg.path[p] = arg.path[p].resolve()
            # verify if the path is valid
            if arg.path[p].is_dir() == False: #Check if paths are valid
                print("Error! You have specified an invalid path.")
                return arg, False
        # if no files or extensions were specified check to not be the current path
        if arg.files == None and arg.fileExtensions == None:
            for path in arg.path:
                # check if the path is the same as current to not load other files by mistake
                if currentPath == path:
                    print("Error! You cant load ALL files from current directory!")
                    return arg, False
        # if files AND extensions are specified check to not have an extension included in the filenames
        if arg.files != None and arg.fileExtensions != None:
            files = []
            for f in arg.files:
                for e in arg.fileExtensions:
                    if f.endswith(e):
                        print("Error, you have specified an extension to a file, while also using extension filter")
                        return arg, False
                    if e[0] != '.': e = '.'+e
                    # make a list with files including their paths for each extension
                    files.extend([pathlib.Path(f+e)])
            # save the list as the files that will be used
            arg.files = files
            del files # get rid of extra data
        # 
        if arg.files != None:
            files = []
            for path in arg.path:
                for f in arg.files:
                    files.extend([path.joinpath(f)])
            arg.files = files
            del files
        #Load all files from the path
        files = []
        for path in arg.path:
            if arg.files == None and arg.fileExtensions != None:
                for ext in arg.fileExtensions:  
                    files.extend([path.joinpath(f) for f in path.glob('*'+ext) if f.is_file()])
            elif arg.files != None:
                files.extend([path.joinpath(f) for f in path.glob('*') if (f in arg.files) and f.is_file()])
            else:
                files.extend([f for f in path.glob('*') if f.is_file()])
        arg.files = files
        del files
    
    # dealing with files when no path is specified
    elif arg.path == None and arg.files != None:
        if arg.fileExtensions != None:
            files = []
            for f in arg.files:
                for e in arg.fileExtensions:
                    if f.endswith(e):
                        print("Error, you have specified an extension to a file, while also using extension filter")
                        return arg, False
                    if e[0] != '.': e = '.'+e
                    files.extend([f+e])
            arg.files = files
            del files
        if arg.files != None:
            for f in range(0,len(arg.files)):
                arg.files[f] = pathlib.Path(arg.files[f])
                if arg.files[f].name.startswith('~/'):
                    arg.files[f] = arg.files[f].expanduser()
                else:
                    arg.files[f] = arg.files[f].resolve()
                if arg.files[f].is_file() == False:
                    print("Eror! You have specified an invalid file.")
                    return arg, False
    elif arg.path == None and arg.fileExtensions != None:
        if arg.fileExtensions != None:
            # get current path
            currentPath = pathlib.Path.cwd()
            files = []
            # get all the available files for each extension from the current path
            for ext in arg.fileExtensions:
                files.extend([f for f in currentPath.glob('*'+ext) if f.is_file()])
            # load all files in the namespace
            arg.files = files
            del files
        else:
            print("Eror! You have to specify at least a file, a path or a file extension.")
            return arg, False
    return arg, True

def checkFiles(arg):
    if len(arg.files) > 0:
        print("Prepared files:")
        for f in arg.files:
            print(f.stem," ", end='', flush = True)
        print("\nStarting the program.\n")
        return True
    else: return False